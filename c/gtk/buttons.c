#include <gtk/gtk.h>

//Set Global Widgets
//Doing this might be frowned uppon :P
GtkWidget *window;
GtkWidget *vbox;
GtkWidget *hello_btn;
GtkWidget *fbk_btn;
GtkWidget *exit_btn;

void print_hello (GtkWidget *widget, gpointer data){
  g_print ("Hello World\n");
}

void print_fbk (){
  gtk_widget_show(exit_btn);
  for(int i=0;i<10;i++){
    g_print ("http://FilmsByKris.com\n");
  }
}

void print_exit(){
  g_print("Goodbye...\n");
}

int main(int argc, char *argv[]) {

  gtk_init(&argc, &argv);

  //create window
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 230, 250);
  gtk_window_set_title(GTK_WINDOW(window), "GtkVBox");
  gtk_container_set_border_width(GTK_CONTAINER(window), 5);

  //Create a Vertical Box 
  //The '1' makes the item expand and fill
  vbox = gtk_box_new(TRUE, 1);
  gtk_container_add(GTK_CONTAINER(window), vbox);

  //////add buttons
  //Hello World Button
  hello_btn = gtk_button_new_with_label ("Hello World");
  g_signal_connect (hello_btn, "clicked", G_CALLBACK (print_hello), NULL);
  gtk_box_pack_start(GTK_BOX(vbox), hello_btn, TRUE, TRUE, 0);


  //FilmsByKris Button
  fbk_btn = gtk_button_new_with_label ("www.FilmsByKris.com");
  g_signal_connect (fbk_btn, "clicked", G_CALLBACK (print_fbk), exit_btn);
  gtk_box_pack_start(GTK_BOX(vbox), fbk_btn, TRUE, TRUE, 0);

  //exit Button
  exit_btn = gtk_button_new_with_label ("exit");
  g_signal_connect (exit_btn, "clicked", G_CALLBACK (print_exit), NULL);
  g_signal_connect_swapped (exit_btn, "clicked", G_CALLBACK (gtk_widget_destroy), window);
  gtk_box_pack_start(GTK_BOX(vbox), exit_btn, TRUE, TRUE, 0);

  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), G_OBJECT(window));

  gtk_widget_show_all(window);
  gtk_widget_hide(exit_btn);

  gtk_main();

  return 0;
}
